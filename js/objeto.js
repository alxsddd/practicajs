automovil = {
    marca:"TOYOTA",
    modelo:"Yaris",
    descripcion: "Toyota Yaris 2024 Automatico",
    imagen: "/img/yaris.png"
}
//Mostrar Objeto
console.log("Automovil: "+ automovil, marca);
console.log("Modelo: "+ automovil, modelo);

function filtrarPorMarca() {
    var select = document.getElementById("marcaSeleccionada");
    var marcaSeleccionada = select.value;
    var cards = document.querySelectorAll(".card");

    cards.forEach(function(card) {
        var marcaCard = card.getAttribute("data-marca");

        if (marcaSeleccionada === "todas" || marcaCard === marcaSeleccionada) {
            card.style.display = "block";
        } else {
            card.style.display = "none";
        }
    });
}